# SQLite Server Python Client

A Python client library for [SQLite Server](https://gitlab.com/bek3/sqlite-server).

**This is a work in progress**

## Installation

Clone this repository as a submodule of your project. Import the sources relatively.

Once this library is stable, it will be available via PyPI.
