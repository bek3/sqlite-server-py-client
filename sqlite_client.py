""" Contains a client class designed to communicate with an SQLite Server
"""


class SQLiteClient:
    """ Instance of SQLite client. Implements the same interface as SQLite connection in Python.
    """

    _url: str

    def __init__(self, url: str):
        """ Constructor

        :param url: URL of server

        """

        if not isinstance(url, str):
            raise TypeError("URL must be a string.")

        self._url = url

    def __repr__(self):
        return "<Client for SQLite server at %s>" % self._url
